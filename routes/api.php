<?php

use App\Http\Controllers\Api\Auth\LoginController;
use App\Http\Controllers\Api\Auth\RegisterController;
use App\Http\Controllers\Api\Auth\ReviewController;
use App\Http\Controllers\Api\Auth\UserController;
use App\Http\Controllers\Api\Reserve\CategoryController;
use App\Http\Controllers\Api\Reserve\OrganizationController;
use App\Http\Controllers\Api\Reserve\PositionController;
use App\Http\Controllers\Api\Reserve\ReserveController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('/register',[RegisterController::class,'register']);
Route::post('/login',[LoginController::class,'login']);

Route::get('/city',[RegisterController::class,'cities']);


Route::middleware('auth:sanctum')->group(function(){
    Route::prefix('/user')->group(function(){
        Route::post('/update', [UserController::class,'updateData']);
        Route::get('/',[UserController::class,'getUser']);
    });

    Route::prefix('/categories')->group(function(){
        Route::get('/',[CategoryController::class,'index']);
    });
    Route::prefix('/favorite')->group(function(){
        Route::get('/',[UserController::class,'favorites']);
        Route::post('/add/{organization}',[OrganizationController::class,'addFavorite']);
    });

    Route::prefix('/reviews')->group(function(){
        Route::post('/',[ReviewController::class,'store']);
    });

    Route::prefix('/organizations')->group(function(){
        Route::get('/',[OrganizationController::class,'index']);
        Route::get('/{organization}',[OrganizationController::class,'show']);
    });

    Route::prefix('/reserves')->group(function(){
        Route::get('/',[ReserveController::class,'index']);
        Route::get('/{reservation}',[ReserveController::class,'show']);
        Route::post('/',[ReserveController::class,'store']);
    });

    Route::prefix('/positions')->group(function(){
        Route::get('/{organization}',[PositionController::class,'index']);
    });
});
