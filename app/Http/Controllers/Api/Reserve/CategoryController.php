<?php

namespace App\Http\Controllers\Api\Reserve;

use App\Http\Controllers\Controller;
use App\Http\Resources\CategoryCollection;
use App\Http\Resources\ShopCategoryCollection;
use App\Models\Category;
use App\Models\ShopCategory;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    //
    public function index()
    {
        return response([
            'categories' => ShopCategory::get()
        ]);
    }
}
