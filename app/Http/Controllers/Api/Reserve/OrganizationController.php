<?php

namespace App\Http\Controllers\Api\Reserve;

use App\Http\Controllers\Controller;
use App\Http\Requests\Organization\FavoriteRequest;
use App\Http\Resources\OrganizationCollection;
use App\Http\Resources\UserCollection;
use App\Models\Organization;
use Illuminate\Http\Request;

class OrganizationController extends Controller
{
    //

    public function index(Request $request)
    {
        $organizations = Organization::where('shop_category_id',$request->category_id)
            ->where('city_id',$request->user()->city_id)
            ->with('city','category')->get();

        foreach ($organizations as $organization){
            $organization['favorite'] = $request->user()->favorites->contains($organization);
        }
        return response([
            'organizations' => $organizations
        ]);
    }


    public function show(Organization $organization)
    {
        return new OrganizationCollection($organization);
    }

    public function addFavorite(Organization $organization,Request $request)
    {
        if(!$request->user()->favorites->contains($organization)){
            $request->user()->favorites()->save($organization);
            $organization['favorite'] = true;

            return response(['favorites' => $organization,'message' => 'Организация добавлена в избранное'],201);

        }else{
            $request->user()->favorites()->detach($organization);
            $organization['favorite'] = false;
            return response(['favorites' => $organization,'message' => 'Организация удалена из избранных '],200);

        }


    }
}
