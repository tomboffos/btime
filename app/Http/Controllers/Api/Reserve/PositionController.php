<?php

namespace App\Http\Controllers\Api\Reserve;

use App\Http\Controllers\Controller;
use App\Models\Organization;
use Illuminate\Http\Request;

class PositionController extends Controller
{
    //
    public function index(Organization $organization)
    {
        return response([
            'positions' => $organization->positions
        ],200);
    }
}
