<?php

namespace App\Http\Controllers\Api\Reserve;

use App\Http\Controllers\Controller;
use App\Http\Requests\ReserveRequest;
use App\Http\Resources\ReservationCollection;
use App\Models\Reservation;
use Illuminate\Http\Request;

class ReserveController extends Controller
{
    //

    public function index(Request $request)
    {
        $reserves = Reservation::where('user_id',$request->user()->id)
            ->with('organization','position')
            ->get();

        return response(['reserves' => $reserves],200);
    }

    public function show(Reservation $reservation)
    {
        return new ReservationCollection($reservation);
    }

    public function store(ReserveRequest $request)
    {
        $data = $request->all();

        $data['user_id'] = $request->user()->id;

        $reserve = Reservation::create($data);


        return response(['reserve' => $reserve]);

    }
}
