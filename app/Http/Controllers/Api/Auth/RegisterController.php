<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Models\City;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    //
    public function register(RegisterRequest $request)
    {
        $data = $request->all();

        $data['role_id'] = 2;
        $data['password'] = Hash::make($data['password']);

        $user = User::create($data);

        $token = $user->createToken(env('APP_NAME'));

        return response([
            'user' => $user,
            'token' => $token->plainTextToken
        ],200);
    }

    public function cities()
    {
        return response(['cities' => City::get()],200);
    }
}
