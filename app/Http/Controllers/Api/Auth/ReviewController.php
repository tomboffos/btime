<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\ReviewRequest;
use App\Http\Resources\ReviewCollection;
use App\Models\Review;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    //
    public function store(ReviewRequest $request)
    {
        $data = $request->all();

        $data['user_id'] = $request->user();

        $review = Review::create($data);

        return new ReviewCollection($review);
    }
}
