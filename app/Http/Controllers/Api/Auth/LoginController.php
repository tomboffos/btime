<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    //
    public function login(UserRequest $request)
    {
        $user = User::where('phone',$request->phone)->first();

        if (Hash::check($request->password,$user->password)){
            $token = $user->createToken(env('APP_NAME'));
            return response([
                'user' => $user,
                'token' => $token->plainTextToken,
                'message' => 'Вы успешно авторизовались'
            ],200);
        }

        return  response([
            'message' => 'Не правильный логин или пароль'
        ],404);
    }
}
