<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateRequest;
use App\Http\Resources\OrganizationCollection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    //
    public function updateData(UpdateRequest $request)
    {
        $data = $request->all();

        if($request->hasFile('avatar'))
            $data['avatar'] = $request->file('avatar')->store('public/users');

        if ($request->has('password') && $request->password != ''){
            $data['password'] = Hash::make($request->password);
        }else{
            unset($data['password']);
        }



        $request->user()->update($data);

        return response([
            'user' => $request->user()
        ]);
    }

    public function getUser(Request $request)
    {
        return response([
            'user' => $request->user()
        ]);
    }

    public function favorites(Request $request)
    {
        $organizations = $request->user()->favorites;
        foreach ($organizations as $organization){
            $organization['favorite'] = $request->user()->favorites->contains($organization);
        }
        return response(['favorites' => $organizations],200);
    }
}
