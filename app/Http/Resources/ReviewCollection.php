<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ReviewCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'user' => new UserCollection($this->user),
            'content' => $this->content,
            'score' => $this->score,
            'organization' => new OrganizationCollection($this->organization)
        ];
    }
}
