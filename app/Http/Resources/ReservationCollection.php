<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ReservationCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'organization' => $this->organization,
            'position_id' => $this->position,
            'start_time' => $this->start_time,
            'date' => $this->date,
            'phone' => $this->phone,

        ];
    }
}
