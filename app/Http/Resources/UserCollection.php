<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class UserCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'email' => $this->email,
            'avatar' => asset('storage/'.$this->avatar),
            'city' => $this->city,
            'phone' => $this->phone,
            'favorites' => OrganizationCollection::collection($this->favorites)
        ];
    }
}
