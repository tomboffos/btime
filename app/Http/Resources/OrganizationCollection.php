<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class OrganizationCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'image' => asset('/storage/'.$this->image),
            'description' => $this->description,
            'city' => $this->city,
            'category' => $this->category,
            'address' => $this->address,
            'price' => $this->minimal_price,
            'rating' => $this->getAverageScore,
            'reviews' => ReviewCollection::collection($this->reviews)
        ];
    }
}
