<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Organization extends Model
{
    use HasFactory,SoftDeletes;

    public function city()
    {
        return $this->belongsTo(City::class);
    }


    public function category()
    {
        return $this->belongsTo(ShopCategory::class,'shop_category_id','id');
    }

    public function reviews()
    {
        return $this->hasMany(Review::class);
    }

    public function getAverageScore()
    {
        return $this->rating/$this->hasMany(Review::class)->count();
    }

    public function positions()
    {
        return $this->hasMany(Position::class,'organization_id','id')->where('is_available',1);
    }

}
